const config = require("./services/config");

const admin = require("firebase-admin");
const functions = require("firebase-functions");
const docusign = require("docusign-esign");
const xmlParser = require("xml2json");
const fetch = require("node-fetch");

const firebase = admin.initializeApp();

// Obtain an OAuth token from https://developers.docusign.com/oauth-token-generator

const accountId = "10856272";

const templateId = "1eb3998f-8e83-4b03-863c-292a1e05be6f";

const BASE_PATH = "https://demo.docusign.net/restapi";

const ROLE_NAMES = {
  donor: "Donor",
  healthProfessional: "Health professional",
};

const authenticationMethod = "None";

const baseUrl = functions.config().web.base;

exports.redirect = functions.https.onRequest(async (request, response) => {
  const code = request.query.code;

  console.log("code", request.query);

  console.log(`${config.docusign.key}:${config.docusign.secret}`);

  const authHeader = getAuthHeader();

  console.log(authHeader);

  const res = await fetch("https://account-d.docusign.com/oauth/token", {
    method: "POST",
    headers: {
      Authorization: `Basic ${authHeader}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({
      "grant_type": "authorization_code",
      code,
    }).toString(),
  }).then((res) => res.json());

  console.log(res);

  response.send("OK");
});

exports.document = functions.https.onRequest(async (request, response) => {
  response.set("Access-Control-Allow-Origin", "*"); // you can also whitelist a specific domain like "http://127.0.0.1:4000"
  response.set("Access-Control-Allow-Headers", "Content-Type");

  const { envelopeId } = request.query;
  const accessToken = await getAccessToken();
  const apiClient = new docusign.ApiClient();
  apiClient.setBasePath(BASE_PATH);
  apiClient.addDefaultHeader("Authorization", "Bearer " + accessToken);
  // Set the DocuSign SDK components to use the apiClient object
  docusign.Configuration.default.setDefaultApiClient(apiClient);
  const envelopesApi = new docusign.EnvelopesApi();
  // call the listDocuments() API
  const { envelopeDocuments } = await envelopesApi.listDocuments(
    accountId,
    envelopeId,
    null
  );

  console.log(envelopeDocuments);

  const { documentId, name } = envelopeDocuments[0];

  console.log(accountId, templateId, documentId);

  const document = await envelopesApi.getDocument(
    accountId,
    envelopeId,
    documentId
  );

  try {
    // response.writeHead(200, {
    //   'Content-Type': "application/pdf",
    //   'Content-disposition': 'attachment;filename=' + name,
    // });
    response.type("application/pdf");
    response.send(Buffer.from(document, "binary"));
  } catch (e) {
    console.log(e);
    response.send(e);
  }
});

exports.hook = functions.https.onRequest((request, response) => {
  response.set("Access-Control-Allow-Origin", "*"); // you can also whitelist a specific domain like "http://127.0.0.1:4000"
  response.set("Access-Control-Allow-Headers", "Content-Type");

  const xmlString = request.body;
  const webhookRequestString = xmlParser.toJson(xmlString);

  const webhookRequest = JSON.parse(webhookRequestString);

  console.log("webhookRequest", webhookRequest);

  console.log("envelopeId", webhookRequest.DocuSignEnvelopeInformation);
  console.log(
    "envelopeId",
    webhookRequest.DocuSignEnvelopeInformation.EnvelopeStatus
  );

  const envelopeId =
    webhookRequest.DocuSignEnvelopeInformation.EnvelopeStatus.EnvelopeID;

  const donorStatus =
    webhookRequest.DocuSignEnvelopeInformation.EnvelopeStatus.RecipientStatuses
      .RecipientStatus[0];
  console.log("donor status", donorStatus.Status);

  const healthProfessionalStatus =
    webhookRequest.DocuSignEnvelopeInformation.EnvelopeStatus.RecipientStatuses
      .RecipientStatus[1];
  console.log("healthcare prof status", healthProfessionalStatus.Status);

  if (
    donorStatus.Status === "Completed" &&
    healthProfessionalStatus.Status !== "Completed"
  ) {
    console.log("setting donor completed");
    admin
      .database()
      .ref("org/envelopes/" + envelopeId + "/donor")
      .update({
        complete: Date.now(),
      });
  }

  if (
    donorStatus.Status === "Completed" &&
    healthProfessionalStatus.Status === "Completed"
  ) {
    console.log("setting healthcare ori completed");
    admin
      .database()
      .ref("org/envelopes/" + envelopeId)
      .update({
        complete: Date.now(),
      });
  }

  response.send("OK");
});

exports.healthProfessional = functions.https.onRequest(
  async (request, response) => {

    response.set("Access-Control-Allow-Origin", "*"); // you can also whitelist a specific domain like "http://127.0.0.1:4000"
    response.set("Access-Control-Allow-Headers", "Content-Type");


    const { envelopeId, healthProfessionalId } = request.query;

    console.log("request.query", request.query);

    const snap = await admin
      .database()
      .ref("org/healthProfessionals/" + healthProfessionalId)
      .once("value");

    const healthProfessional = snap.val();

    console.log("healthProfessional", healthProfessional);
    const accessToken = await getAccessToken();

    const apiClient = new docusign.ApiClient();
    apiClient.setBasePath(BASE_PATH);
    apiClient.addDefaultHeader("Authorization", "Bearer " + accessToken);
    // Set the DocuSign SDK components to use the apiClient object
    docusign.Configuration.default.setDefaultApiClient(apiClient);
    const envelopesApi = new docusign.EnvelopesApi();

    try {
      const recipients = await envelopesApi.listRecipients(
        accountId,
        envelopeId
      );
      console.log("RECIPIENTS", recipients);
      // const res = await fetch(
      //   `${BASE_PATH}/v2.1/accounts/${accountId}/envelopes/${envelopeId}/recipients`,
      //   {
      //     body: {
      //       signers: [
      //         {
      //           recipientId: "2",
      //           roleName: ROLE_NAMES.healthProfessional,
      //           userName: healthProfessional.fullName,
      //           email: healthProfessional.email,
      //           clientUserId: healthProfessional.id,
      //         },
      //       ],
      //     },
      //     headers: [["Authorization", "Bearer " + accessToken]],
      //   }
      // ).then(res => res.json());
      //
      // console.log(res);

      const res = await envelopesApi.updateRecipients(accountId, envelopeId, {
        recipients: {
          signers: [
            {
              recipientId: "2",
              roleName: ROLE_NAMES.healthProfessional,
              userName: healthProfessional.fullName,
              name: healthProfessional.fullName,
              fullName: healthProfessional.fullName,
              email: healthProfessional.email,
              clientUserId: healthProfessional.id,
            },
          ],
        },
      });

      const recipientViewRequest = docusign.RecipientViewRequest.constructFromObject(
        {
          authenticationMethod: authenticationMethod,
          clientUserId: healthProfessionalId,
          recipientId: "2",
          returnUrl: baseUrl + "/dashboard",
          userName: healthProfessional.fullName,
          email: healthProfessional.email,
        }
      );

      const results = await envelopesApi.createRecipientView(
        accountId,
        envelopeId,
        {
          recipientViewRequest: recipientViewRequest,
        }
      );

      response.send({ url: results.url });
    } catch (e) {
      console.log("Error", e);
      response.send(e);
    }
  }
);

exports.donor = functions.https.onRequest(async (request, response) => {

  const { fullName, dob, email, uid } = request.query;

  response.set("Access-Control-Allow-Origin", "*"); // you can also whitelist a specific domain like "http://127.0.0.1:4000"
  response.set("Access-Control-Allow-Headers", "Content-Type");

  const envelopeDefinition = new docusign.EnvelopeDefinition();
  envelopeDefinition.accountId = accountId;
  envelopeDefinition.emailSubject = "Donor questionnaire";
  envelopeDefinition.templateId = templateId;
  envelopeDefinition.status = "sent"; // sent immediately

  // Create template role elements to connect the signer and cc recipients
  // to the template
  // We're setting the parameters via the object creation
  const donorRole = docusign.TemplateRole.constructFromObject({
    name: fullName,
    email,
    clientUserId: uid,
    roleName: ROLE_NAMES.donor,
    tabs: {
      textTabs: [
        {
          tabLabel: "dob",
          value: dob,
        },
      ],
    },
  });

  const healthProfessionalRole = docusign.TemplateRole.constructFromObject({
    name: "Health Professional",
    clientUserId: "1",
    email: "placeholder@example.com",
    roleName: ROLE_NAMES.healthProfessional,
  });

  envelopeDefinition.templateRoles = [donorRole, healthProfessionalRole];

  envelopeDefinition.eventNotification = {
    url: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/hook",
    loggingEnabled: "true",
    requireAcknowledgment: "false",
    envelopeEvents: [
      { envelopeEventStatusCode: "sent" },
      { envelopeEventStatusCode: "delivered" },
      { envelopeEventStatusCode: "completed" },
      { envelopeEventStatusCode: "declined" },
      { envelopeEventStatusCode: "voided" },
    ],
    recipientEvents: [
      { recipientEventStatusCode: "Sent" },
      { recipientEventStatusCode: "Delivered" },
      { recipientEventStatusCode: "Completed" },
      { recipientEventStatusCode: "Declined" },
      { recipientEventStatusCode: "AuthenticationFailed" },
      { recipientEventStatusCode: "AutoResponded" },
    ],
    useSoapInterface: "false",
    includeDocuments: "false",
    includeCertificateWithSoap: "false",
    signMessageWithX509Cert: "false",
  };
  const accessToken = await getAccessToken();
  console.log(accessToken);
  const apiClient = new docusign.ApiClient();
  apiClient.setBasePath(BASE_PATH);
  apiClient.addDefaultHeader("Authorization", "Bearer " + accessToken);
  // Set the DocuSign SDK components to use the apiClient object
  docusign.Configuration.default.setDefaultApiClient(apiClient);
  const envelopesApi = new docusign.EnvelopesApi();

  let results;

  try {
    results = await envelopesApi.createEnvelope(accountId, {
      envelopeDefinition: envelopeDefinition,
    });

    const envelopeId = results.envelopeId;
    const recipientViewRequest = docusign.RecipientViewRequest.constructFromObject(
      {
        authenticationMethod: authenticationMethod,
        clientUserId: uid,
        recipientId: "1",
        returnUrl: baseUrl + "/donor/complete",
        userName: fullName,
        email: email,
      }
    );

    results = await envelopesApi.createRecipientView(accountId, envelopeId, {
      recipientViewRequest: recipientViewRequest,
    });

    firebase
      .database()
      .ref("org/envelopes/" + envelopeId)
      .set({
        envelopeId,
        created: Date.now(),
        donor: {
          uid,
          fullName,
          dob,
        },
      });

    response.send({ url: results.url });
  } catch (e) {
    console.log("Error", e);
    response.status(e.status).send(e.response);
  }
});


async function getAccessToken() {

  const snap = await admin
    .database()
    .ref("org/refreshToken")
    .once("value");

  const refreshToken = snap.val();

  console.log("refresh_token", refreshToken);

  const authHeader = getAuthHeader();

  const res = await fetch("https://account-d.docusign.com/oauth/token", {
    method: "POST",
    headers: {
      Authorization: `Basic ${authHeader}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({
      "grant_type": "refresh_token",
      "refresh_token": refreshToken,
    }).toString(),
  }).then((res) => res.json());

  console.log(res);

  return res.access_token;


}

function getAuthHeader() {
  return Buffer.from(
    `${config.docusign.key}:${config.docusign.secret}`
  ).toString("base64");


}
