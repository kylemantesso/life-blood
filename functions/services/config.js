const functions =  require("firebase-functions");

const config = {
  web: {
    base: functions.config().web.base
  },
  docusign: {
    secret: functions.config().docusign.secret,
    key: functions.config().docusign.key,
  },
};

module.exports = config;
