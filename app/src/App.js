import React, { useState } from "react";
import { Box, Grommet, Header, Heading, Menu, Text } from "grommet";
import { theme } from "./theme";
import { Favorite, Language } from "grommet-icons";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { DonorForm } from "./components/DonorForm";
import { Dashboard } from "./components/Dashboard";
import { DonorComplete } from "./components/DonorComplete";
import { HealthProfessionalComplete } from "./components/HealthProfessionalComplete";
import { useTranslation } from "react-i18next";
import { Auth } from "./components/Auth";
import {AuthComplete} from "./components/AuthComplete";

export const App = () => {
  const [t, i18n] = useTranslation("common");
  const [label, setLabel] = useState("English");

  return (
    <Grommet full theme={theme}>
      <Header background="brand" pad="medium">
        <Box direction="row" align="center" gap="small">
          <Favorite size="large" />
          <Heading size="small" margin="none">
            LifeBank
          </Heading>
        </Box>
        {window.location.pathname.includes("dashboard") ? (
          <Box direction="row" align="center" gap="medium">
            <Text>Dr Teresa Miller</Text>
            <img src="profile.png" height="48" />
          </Box>
        ) : (
          <Menu
            icon={<Language />}
            label={label}
            items={[
              {
                label: "English",
                onClick: () => {
                  setLabel("English");
                  i18n.changeLanguage("en");
                },
              },
              {
                label: "Indonesian",
                onClick: () => {
                  setLabel("Indonesian");
                  i18n.changeLanguage("id");
                },
              },
              {
                label: "Hindi",
                onClick: () => {
                  setLabel("Hindi");
                  i18n.changeLanguage("hi");
                },
              },
            ]}
          />
        )}
      </Header>
      <Router>
        <Switch>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/donor/complete">
            <DonorComplete />
          </Route>
          <Route path="/auth/complete">
            <AuthComplete />
          </Route>
          <Route path="/auth">
            <Auth />
          </Route>
          <Route path="/hp/complete">
            <HealthProfessionalComplete />
          </Route>
          <Route path="/">
            <DonorForm />
          </Route>
        </Switch>
      </Router>
    </Grommet>
  );
};

export default App;
