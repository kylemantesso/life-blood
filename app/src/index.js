import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import firebase from "firebase";
import { I18nextProvider } from "react-i18next";
import i18next from "i18next";
import common_id from "./translations/id/common.json";
import common_en from "./translations/en/common.json";
import common_hi from "./translations/hi/common.json";

i18next.init({
  interpolation: { escapeValue: false },
  lng: "en",
  resources: {
    en: {
      common: common_en,
    },
    id: {
      common: common_id,
    },
    hi: {
      common: common_hi
    }
  },
});
const firebaseConfig = {
  apiKey: "AIzaSyBawY3LyG0SPpQqwS4uF0ClJZ-nGFtyeKc",
  authDomain: "docusign-hack-f577b.firebaseapp.com",
  databaseURL: "https://docusign-hack-f577b.firebaseio.com",
  projectId: "docusign-hack-f577b",
  storageBucket: "docusign-hack-f577b.appspot.com",
  messagingSenderId: "1092096917973",
  appId: "1:1092096917973:web:4010e7c4fc7887ab32fd5c",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

i18next.init({
  interpolation: { escapeValue: false }, // React already does escaping
});

ReactDOM.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18next}>
      <App />
    </I18nextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
