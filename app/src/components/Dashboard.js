import React, { useEffect, useState } from "react";
import { Page, PageWide } from "./Page";
import * as firebase from "firebase";
import {
  Button,
  Heading,
  Paragraph,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableRow,
  Tabs,
} from "grommet";
import TimeAgo from "react-timeago";
import { config } from "../config";
import { Spinner } from "./Spinner";
import {DownloadButton} from "./DownloadButton";

export const ReadyTable = ({ envelopes }) => {
  const [loading, setLoading] = useState(false);
  const handleReview = async ({ healthProfessionalId, envelopeId }) => {
    setLoading(true);
    const { url } = await fetch(
      `${config.firebase.functions.healthProfessional}?healthProfessionalId=${healthProfessionalId}&envelopeId=${envelopeId}`
    ).then((res) => res.json());

    console.log(url);

    window.location = url;
  };

  return (
    <>
      {loading ? (
        <Spinner />
      ) : (
        <Table margin={{ top: "large" }} style={{ width: "100%" }}>
          <TableHeader>
            <TableRow>
              <TableCell scope="col" border="bottom">
                Name
              </TableCell>
              <TableCell scope="col" border="bottom">
                Donor Signed
              </TableCell>
              <TableCell scope="col" border="bottom" />
            </TableRow>
          </TableHeader>
          <TableBody>
            <>
              {envelopes &&
                envelopes.map((envelope) => (
                  <TableRow key={envelope.envelopeId}>
                    <TableCell>{envelope.donor.fullName}</TableCell>
                    <TableCell>
                      <TimeAgo date={envelope.donor.complete} />
                    </TableCell>
                    <TableCell>
                      <Button
                        label="Review"
                        onClick={() =>
                          handleReview({
                            healthProfessionalId: "12345",
                            envelopeId: envelope.envelopeId,
                          })
                        }
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </>
          </TableBody>
        </Table>
      )}
    </>
  );
};

export const CompleteTable = ({ envelopes }) => {
  return (
    <Table margin={{ top: "large" }} style={{ width: "100%" }}>
      <TableHeader>
        <TableRow>
          <TableCell scope="col" border="bottom">
            Name
          </TableCell>
          <TableCell scope="col" border="bottom">
            Completed
          </TableCell>
          <TableCell scope="col" border="bottom" />
        </TableRow>
      </TableHeader>
      <TableBody>
        <>
          {envelopes &&
            envelopes.map((envelope) => (
              <TableRow key={envelope.envelopeId}>
                <TableCell>{envelope.donor.fullName}</TableCell>
                <TableCell>
                  {new Date(envelope.donor.complete).toLocaleString()}
                </TableCell>
                <TableCell>
                  <DownloadButton envelopeId={envelope.envelopeId} />
                </TableCell>
              </TableRow>
            ))}
        </>
      </TableBody>
    </Table>
  );
};

export const UnsignedTable = ({ envelopes }) => {
  return (
    <Table margin={{ top: "large" }} style={{ width: "100%" }}>
      <TableHeader>
        <TableRow>
          <TableCell scope="col" border="bottom">
            Name
          </TableCell>
          <TableCell scope="col" border="bottom">
            Donor Submitted
          </TableCell>
        </TableRow>
      </TableHeader>
      <TableBody>
        <>
          {envelopes &&
            envelopes.map((envelope) => (
              <TableRow key={envelope.envelopeId}>
                <TableCell>{envelope.donor.fullName}</TableCell>
                <TableCell>
                  <TimeAgo date={envelope.donor.created} />
                </TableCell>
                <TableCell>
                  <TimeAgo date={envelope.complete} />
                </TableCell>
              </TableRow>
            ))}
        </>
      </TableBody>
    </Table>
  );
};

export const Dashboard = () => {
  const [envelopes, setEnvelopes] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const ref = firebase.database().ref("org/envelopes");
    ref.on("value", (snap) => {
      const allEnvelopes = snap.val();
      console.log(allEnvelopes);

      let unsigned = [],
        signed = [],
        complete = [];

      if (allEnvelopes) {
        Object.values(allEnvelopes).forEach((envelope) => {
          if (!envelope.donor.complete) {
            unsigned.push(envelope);
          } else if (envelope.complete) {
            complete.push(envelope);
          } else {
            signed.push(envelope);
          }
        });

        setEnvelopes({ unsigned, signed, complete });
      }
      setLoading(false);
    });
    return () => {
      ref.off();
    };
  }, []);

  return (
    <PageWide pad="small">
      <Heading>Dashboard</Heading>
      {!loading && envelopes && (
        <Tabs alignControls={"start"}>
          <Tab title="Ready">
            <ReadyTable envelopes={envelopes.signed} />
          </Tab>
          <Tab title="Unsigned">
            <UnsignedTable envelopes={envelopes.unsigned} />
          </Tab>
          <Tab title="Complete">
            <CompleteTable envelopes={envelopes.complete} />
          </Tab>
        </Tabs>
      )}
      {!loading && !envelopes && (
        <Paragraph>No donor forms have been submitted</Paragraph>
      )}
      {loading && <Spinner />}
    </PageWide>
  );
};
