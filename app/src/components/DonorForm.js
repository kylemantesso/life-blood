import React, { useState } from "react";
import {
  Box,
  Button,
  FormField,
  Paragraph,
  TextInput,
  Form,
  MaskedInput,
  Heading,
} from "grommet";
import { Page } from "./Page";
import { config } from "../config";
import { Spinner } from "./Spinner";
import { useTranslation } from "react-i18next";

const daysInMonth = (month) => new Date(2020, month, 0).getDate();

export const DonorForm = () => {
  const { t } = useTranslation("common");

  const [dob, setDob] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async ({ fullName, email }) => {
    setLoading(true);
    console.log(fullName, email, dob);
    const { url } = await fetch(
      `${config.firebase.functions.donor}?fullName=${fullName}&dob=${dob}&email=${email}&uid=123`
    ).then((res) => res.json());

    window.location = url;
  };

  return (
    <Page>
      {loading ? (
        <Spinner />
      ) : (
        <Box background="white" pad="large">
          <Heading margin="none" size="small">
            {t("donorForm.title")}
          </Heading>
          <Paragraph>{t("donorForm.description")}</Paragraph>
          <Form onSubmit={({ value }) => handleSubmit(value)}>
            <FormField name="fullName" label={t("donorForm.fullName.title")}>
              <TextInput
                required
                name="fullName"
                placeholder={t("donorForm.fullName.placeholder")}
              />
            </FormField>
            <FormField name="email" label={t("donorForm.email.title")}>
              <TextInput
                required
                type="email"
                name="email"
                placeholder={t("donorForm.email.placeholder")}
              />
            </FormField>
            <FormField name="dob" label={t("donorForm.dob.title")}>
              <MaskedInput
                name="dob"
                required
                mask={[
                  {
                    length: [1, 2],
                    options: Array.from(
                      {
                        length: daysInMonth(parseInt(dob.split("/")[0], 10)),
                      },
                      (v, k) => k + 1
                    ),
                    regexp: /^[1-2][0-9]$|^3[0-1]$|^0?[1-9]$|^0$/,
                    placeholder: "dd",
                  },
                  { fixed: "/" },
                  {
                    length: [1, 2],
                    options: Array.from({ length: 12 }, (v, k) => k + 1),
                    regexp: /^1[0,1-2]$|^0?[1-9]$|^0$/,
                    placeholder: "mm",
                  },
                  { fixed: "/" },
                  {
                    length: 4,
                    options: Array.from({ length: 100 }, (v, k) => 2019 - k),
                    regexp: /^[1-2]$|^19$|^20$|^19[0-9]$|^20[0-9]$|^19[0-9][0-9]$|^20[0-9][0-9]$/,
                    placeholder: "yyyy",
                  },
                ]}
                value={dob}
                onChange={(event) => setDob(event.target.value)}
              />
            </FormField>
            <Box direction="row" gap="medium">
              <Button type="submit" primary label={t("donorForm.submit")} />
            </Box>
          </Form>
        </Box>
      )}
    </Page>
  );
};
