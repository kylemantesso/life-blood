import {Box, Button, Paragraph} from "grommet";
import React from "react";
import {Page} from "./Page";
import {config} from "../config";

const INTEGRATION_KEY = "73957d6a-7cb5-43a0-8540-85d6039c66d7";

export const Auth = () => {

  console.log(config.firebase.FUNCTION_URL)

  return (
    <Page pad="small">
      <Paragraph>Authorise Life Bank to access my Docusign account?</Paragraph>
      <Box direction="row">
        <Button primary label="Authorize" onClick={() => {
          window.location.href = `https://account-d.docusign.com/oauth/auth?response_type=code&scope=extended%20signature&client_id=${INTEGRATION_KEY}&redirect_uri=${config.firebase.functions.redirect}`
        }}/>
      </Box>
    </Page>
  )
};
