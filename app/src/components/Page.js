import React from "react";
import styled from "styled-components";
import { Main } from "grommet";

export const Page = styled(Main)`
  max-width: 580px;
  margin-top: 24px;
  margin-left: auto;
  margin-right: auto;
`;

export const PageWide = styled(Main)`
  max-width: 720px;
  margin-top: 24px;
  margin-left: auto;
  margin-right: auto;
`;
