import React from "react";
import {
  Box,
  Button,
  FormField,
  Paragraph,
  TextInput,
  Form,
  MaskedInput,
} from "grommet";
import { Page } from "./Page";
import { config } from "../config";

export const HealthProfessionalComplete = () => {
  return (
    <Page pad="small" fill>
      <Paragraph>Thanks for completing the form</Paragraph>
    </Page>
  );
};
