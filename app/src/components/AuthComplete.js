import {Box, Button, Paragraph} from "grommet";
import React from "react";
import {Page} from "./Page";
import {config} from "../config";

const INTEGRATION_KEY = "73957d6a-7cb5-43a0-8540-85d6039c66d7";

export const AuthComplete = () => {
  return (
    <Page pad="small">
      <Paragraph>Thanks! Your DocuSign account is now authorized</Paragraph>

    </Page>
  )
};
