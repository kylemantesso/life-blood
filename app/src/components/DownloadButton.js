import {config} from "../config";
import {Button} from "grommet";
import React, {useState} from "react";
import {SpinnerSvg, SpinnerSvgSmall} from "./Spinner";


export const DownloadButton = ({envelopeId}) => {

  const [loading, setLoading] = useState(false);

  return <Button
    icon={loading ? SpinnerSvgSmall : null}
    label="Download"
    disabled={loading}
    onClick={() => {
      setLoading(true);
      fetch(
        `${config.firebase.functions.document}?envelopeId=${envelopeId}`
      ).then((res) =>
        res.blob().then((blob) => {
          console.log(blob);
          const url = window.URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', 'donor_form.pdf');
          document.body.appendChild(link);
          link.click();
          setLoading(false);
        })
      );
    }}
  />
}
