

export const ENV = process.env.NODE_ENV;

let config;

if(ENV === "production") {
  config = {
    firebase: {
      functions: {
        document: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/document",
        donor: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/donor",
        healthProfessional: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/healthProfessional",
        hook: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/hook",
        redirect: "https://us-central1-docusign-hack-f577b.cloudfunctions.net/redirect",
      },
    }}
} else {
  config = {
    firebase: {
      functions: {
        document: "http://localhost:5001/docusign-hack-f577b/us-central1/document",
        donor: "http://localhost:5001/docusign-hack-f577b/us-central1/donor",
        healthProfessional: "http://localhost:5001/docusign-hack-f577b/us-central1/healthProfessional",
        hook: "http://localhost:5001/docusign-hack-f577b/us-central1/hook",
        redirect: "http://localhost:5001/docusign-hack-f577b/us-central1/redirect",
      },
    }
  }
}

export { config };
